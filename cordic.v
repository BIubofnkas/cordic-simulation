/*
fixed point constant numbers from C program
K is 39796
angle 1 is 51472
angle 2 is 30386
angle 3 is 16055
angle 4 is 8150
angle 5 is 4091
angle 6 is 2047
angle 7 is 1024
angle 8 is 512
angle 9 is 256
angle 10 is 128
angle 11 is 64
angle 12 is 32
angle 13 is 16
angle 14 is 8
angle 15 is 4
angle 16 is 2
*/

module CORDIC(output signed[1:-16] cosine,
			  output signed[1:-16] sine,
			  output done,
			  input signed[1:-16] target_angle,
			  input init, clk);
	reg signed [17:0] C;	
	reg signed [17:0] S;
	reg signed [17:0] C_saved;	
	reg signed [17:0] S_saved;
	reg signed [17:0] C_old;
	reg signed [17:0] S_old;
	reg signed [17:0] angle;
	reg signed [17:0] target;
	reg signed [17:0] angles;
	reg [17:0] K; //constant K
	reg [3:0] counter;
	reg [2:0] state;  
	//INIT, RUNNING, DONE

	initial
	begin	
		K = 18'd39796;
	end
	
	//initialise constants
	always @(*)
	begin
		case (counter)				
		4'd0: angles = 18'd51472;
		4'd1: angles = 18'd30386;
		4'd2: angles = 18'd16055;
		4'd3: angles = 18'd8150;
		4'd4; angles = 18'd4091;
		4'd5: angles = 18'd2047;
		4'd6: angles = 18'd1024;
		4'd7: angles = 18'd512;
		4'd8: angles = 18'd256;
		4'd9: angles = 18'd128;
		4'd10: angles = 18'd64;
		4'd11: angles = 18'd32;
		4'd12: angles = 18'd16;
		4'd13: angles = 18'd8;
		4'd14: angles = 18'd4;
		4'd15: angles = 18'd2;
		endcase
	end
	//Change output on positive clock edge
	always @(posedge clk) 
	begin
		if (init)
		begin
			counter <= 4'd0;
			done <= 1'd0;		
			C <= 18'd0;
			S <= 18'd0;
			if (target >= 18'd0)
			begin
				angle <= angle;
				C_old <= K;
				S_old <= -K;
			end
			else
			begin
				angle <= ~angle + 1; //-angle two's complement
				C_old <= K;
				S_old <= -K;
			end
		end
		else
		begin
			if (!done)
			begin
				if (counter <= 4'hF)
				begin
					C_old <= C;
					S_old <= S;
					if (angle <= target)
						begin
						angle += angles; //auto modified to table lookup see @* block
						C = C_old + S_old >> counter;
						S = S_old - C_old >> counter;				
						end
				
					else
						begin				
						angle += angles; //auto modified to table lookup see @* block
						C = C_old + S_old >> counter;
						S = S_old - C_old >> counter;
						end
					counter <= counter + 1;		
				end
				else
				begin
					done <= 1;
				end				
			end
			else
			begin
				C_saved <= C;
				S_saved <= S;
				init <= 1'd0;
				done <= 1'd0;
			end
		end
	end
endmodule
